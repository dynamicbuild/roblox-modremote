return {
	Name = "ModRemote",
	Version = "4.0",
	Description = "Easy ROBLOX Networking",
	Author = {
		Name = "thehoudeks",
		UserId = 66355056
	},
	License = "MIT",
	Main = "ModRemote",
	Ignore = {
		"API.md",
		"LICENSE",
		"README.md",
	},
	Platform = {
		Server = true;
		Client = true
	},
	Dependencies = {
	}
}